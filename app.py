from flask import Flask
from flask_apscheduler import APScheduler


scheduler = APScheduler()

from task import *

class Config(object):
  SCHEDULER_API_ENABLED = True
  SCHEDULER_TIMEZONE = 'Asia/Bangkok'

if __name__ == '__main__':
  app = Flask(__name__)
  app.config.from_object(Config())
  scheduler.init_app(app)
  scheduler.start()
  app.run()
