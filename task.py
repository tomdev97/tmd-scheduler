from helpers import getNeededDataFromUrl, writeDataToCsvFile, readDataFromCsvFile, concatFiles
from config import url
from datetime import datetime
from app import scheduler
import os

# This function will run at 2:00, 5:00, 8:00, 11:00, 14:00, 17:00, 20:00, 23:00 GMT+7
# After 1 hour when Weather3Hours is updated, it will collect data and write to a CSV file
# Output: CSV file contains Tmd's Weather3Hours data for each timeline.
@scheduler.task('cron', id='takeDataFromTmd', hour='2,5,8,11,14,17,20,23')
def jobRunEvery3Hours():
  try:
    print('start')
    data = getNeededDataFromUrl(url)
    writeDataToCsvFile(data)
    print('done')
    return True
  except Exception as e:
    raise e


# This function will run at 23:30 GMT+7
# It will concat 8 csv files above into 1 file with only have 5 columns
# Output: CSV file contains Tmd's Weather3Hours data for a day.
@scheduler.task('cron', id='concatDataFromTmd', hour='23', minute='30')
def jobRunAtTheEndOfDay():
  try:
    print('start')
    allData = []
    for ele in [2,5,8,11,14,17,20,23]:
      dateString = datetime.strftime(datetime.now(), '%d-%m-%Y')
      dateString = f'{ele}_{dateString}'
      if not os.path.isfile(f'./csv/{dateString}.csv'):
        continue
      csvData = readDataFromCsvFile(dateString)
      for row in csvData:
        data = {}
        data['DateTime'] = row['DateTime']
        data['StationPressure'] = float(row['StationPressure']) * 0.75
        data['MeanSeaLevelPressure'] = float(row['MeanSeaLevelPressure']) * 0.75
        data['AirTemperature'] = (float(row['AirTemperature']) * 9 / 5) + 32
        data['DewPoint'] = (float(row['DewPoint']) * 9 / 5) + 32
        allData.append(data)
    concatFiles(allData)
    print('done')
    return True
  except Exception as e:
    raise e
