# This function make sure the consistency of Weather3Hours data if the data is missing
def convertStrToFloat(key):
  try:
    if type(key) is str:
      res = float(key)
    else:
      res = 0
  except:
    res = 0
  return res
