from datetime import datetime
import csv
import os

# Write the data collected from TMD into CSV file
def writeDataToCsvFile(data):
  try:
    with open(f"{os.path.dirname(__file__)}/../csv/{datetime.strftime(datetime.now(), '%H_%d-%m-%Y')}.csv", mode='a') as csvFile:
      fieldnames = ['DateTime', 'StationPressure', 'MeanSeaLevelPressure', 'AirTemperature', 'DewPoint', 'LandVisibility', 'WindDirection', 'WindSpeed', 'Rainfall', 'Rainfall24Hr']
      writer = csv.DictWriter(csvFile, fieldnames=fieldnames)

      writer.writeheader()
      writer.writerows(data)
    return True
  except Exception as e:
    raise e

# Read the data from CSV file
def readDataFromCsvFile(dateString):
  try:
    res = []
    with open(f'{os.path.dirname(__file__)}/../csv/{dateString}.csv', mode='r') as csvFile:
      res = list(csv.DictReader(csvFile))
    return res
  except Exception as e:
    raise e

# Concat multiple files into 1 file at the end of the day
def concatFiles(data):
  try:
    if (data == None):
      return True
    with open(f"{os.path.dirname(__file__)}/../csv/{datetime.strftime(datetime.now(), '%d-%m-%Y')}.csv", mode='a') as csvFile:
      fieldnames = ['DateTime', 'StationPressure', 'MeanSeaLevelPressure', 'AirTemperature', 'DewPoint']
      writer = csv.DictWriter(csvFile, fieldnames=fieldnames)

      writer.writeheader()
      writer.writerows(data)
    return True
  except Exception as e:
    raise e
