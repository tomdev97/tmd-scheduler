import urllib.request
import json
import collections
from .convertStrToFloat import  convertStrToFloat

# Take data from TMD and put it into a dictionary
def getNeededDataFromUrl(url):
  res = []
  req = urllib.request.Request(url, None)

  dataHtml = urllib.request.urlopen(req).read()
  dataJson = json.loads(dataHtml.decode(
    'utf-8-sig'), object_pairs_hook=collections.OrderedDict)

  if len(dataJson) == 0:
    return None

  stations = dataJson['Stations']['Station']
  for stationData in stations:
    data = {}
    obs = stationData['Observation']
    data['DateTime'] = obs['DateTime']
    data['StationPressure'] = convertStrToFloat(obs['StationPressure'])
    data['MeanSeaLevelPressure'] = convertStrToFloat(obs['MeanSeaLevelPressure'])
    data['AirTemperature'] = convertStrToFloat(obs['AirTemperature'])
    data['DewPoint'] = convertStrToFloat(obs['DewPoint'])
    data['LandVisibility'] = convertStrToFloat(obs['LandVisibility'])
    data['WindDirection'] = convertStrToFloat(obs['WindDirection'])
    data['WindSpeed'] = convertStrToFloat(obs['WindSpeed'])
    data['Rainfall'] = convertStrToFloat(obs['Rainfall'])
    data['Rainfall24Hr'] = convertStrToFloat(obs['Rainfall24Hr'])
    res.append(data)

  return res
  