## Description
TMD scheduler APIs

## Installation
```bash
$ sudo apt-get install python3-pip
$ sudo update
$ pip3 install -r requirements.txt
```

*Install Python3 on mac
```bash
$ brew install python3
$ curl https://bootstrap.pypa.io/get-pip.py | python3
$ python3
$ Then Use pip3 to install modules
```

* pylint extension might be good for you

## Running the app

```bash
$ python3 app.py
```